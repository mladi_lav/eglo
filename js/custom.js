$(document).ready(function () {
    mainSlider();
    productSlider();
    viewedSlider();
    $( "select" ).selectmenu();
});


function viewedSlider() {
    var mySwiper = new Swiper ('.viewed-container', {
            slidesPerView: 'auto',
            scrollbarHide: false,
            nextButton: '.viewed-next',
            prevButton: '.viewed-prev',
            scrollbar: '.swiper-scrollbar'
        })
}

function productSlider() {
    var galleryTop = new Swiper('.gallery-top', {
        nextButton: '.gallery-button-next',
        prevButton: '.gallery-button-prev',
        spaceBetween: 10
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 20,
        centeredSlides: true,
        slidesPerView: 'auto',
        touchRatio: 0.2,
        slideToClickedSlide: true
    });
    galleryTop.params.control = galleryThumbs;
    galleryThumbs.params.control = galleryTop;
}

function mainSlider() {
    var mySwiper = new Swiper ('.main-slider', {
        loop: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'

    })
}